const { join } = require("path");

/**
 * @type {import("next").NextConfig}
 */
const nextConfig = {
  output: "export",
  images: {
    unoptimized: true,
    domains: ["localhost"],
  },
  poweredByHeader: false,
  reactStrictMode: false,
  sassOptions: {
    includePaths: [join(__dirname, "styles")],
  },
  trailingSlash: true,
};

module.exports = nextConfig;
