import GeneralMeta from "../components/general-meta";
import Layout from "../components/layout";
import OpenGraphArticle from "../components/open-graph-article";
import Title from "../components/title";
import { CommonPage, getCommonPageStaticProps } from "../lib/common-page";
import { pageUrl } from "../lib/fns";

export const path = "/startovka/";
export const title = "Startovní listina 11. ročníku";

const startovka: CommonPage = ({ raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta canonical={pageUrl(path)} />
    <OpenGraphArticle title={title} url={pageUrl(path)} />
    <article className="p-3 my-3 bg-light rounded-3">
      <h1 className="text-center font-weight-bold my-3">
        {title}
      </h1>
      <div
        dangerouslySetInnerHTML={{
          __html: `
<div id="divRRPublish" class="RRPublish"></div>
<script src="https://my.raceresult.com/RRPublish/load.js.php?lang=cs"></script>
<script>
var rrp = new RRPublish(document.getElementById("divRRPublish"), 299916, "participants");
rrp.ShowTimerLogo = true;
rrp.ShowInfoText = false;
</script>`,
        }}
      />
    </article>
  </Layout>
);

export default startovka;

export const getStaticProps = getCommonPageStaticProps;
