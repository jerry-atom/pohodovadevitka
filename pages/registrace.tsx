import GeneralMeta from "../components/general-meta";
import Layout from "../components/layout";
import OpenGraphArticle from "../components/open-graph-article";
import Title from "../components/title";
import { CommonPage, getCommonPageStaticProps } from "../lib/common-page";
import { pageUrl } from "../lib/fns";

export const path = "/registrace/";
export const title = "Registrace na 11. ročník";

const registrace: CommonPage = ({ raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta canonical={pageUrl(path)} />
    <OpenGraphArticle title={title} url={pageUrl(path)} />
    <article className="p-3 my-3 bg-light rounded-3">
      <h1 className="text-center font-weight-bold my-3">
        {title}
      </h1>
      <div
        dangerouslySetInnerHTML={{
        __html: `<script>
var RRReg_eventid = "299916";
var RRReg_name = "Single Registration";
var RRReg_key = "EN339kacqlFU";
var RRReg_server = "https://events2.raceresult.com";
var RRReg_lang = "cz";
</script>
<script src="https://events2.raceresult.com/registrations/init.js?lang=cz&build=v12.5.25-5"></script>`,
        }}
      />
    </article>
  </Layout>
);

export default registrace;

export const getStaticProps = getCommonPageStaticProps;
