import { GetStaticProps } from "next";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

import { path as registraceUrl } from "../pages/registrace";
import Layout, { Props as LayoutProps } from "../components/layout";
import { site } from "../lib/constants";
import logoBig from "../public/images/logo_big.jpg";
import mapa from "../public/files/mapa-2018.jpg";
import tunel from "../public/images/tunel-2024.jpg";
import { fileUrl, imageUrl, pageUrl } from "../lib/fns";
import GeneralMeta from "../components/general-meta";
import OpenGraphArticle from "../components/open-graph-article";
import Title from "../components/title";
import { getRaceEventData, getRaceEventHeaders, RaceEventData } from "../lib/race-events";
import Categories from "../components/categories";

export const path = "/propozice/";
export const title = "Propozice";

type Props = LayoutProps & {
  data: RaceEventData;
};

const Propozice: FC<Props> = ({ data, raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta canonical={pageUrl(path)} />
    <OpenGraphArticle title={title} url={pageUrl(path)} />

    <div className="btn-toolbar d-flex justify-content-end d-print-none" role="toolbar">
      <Link
        href={fileUrl("Propozice-2024.pdf")}
        className="btn btn-sm btn-success ms-2"
        target="_blank"
        rel="noopener"
        title="Stáhnout propozice jako PDF"
      >
        <i className="bi-download me-2" />
        Stáhnout PDF
      </Link>
    </div>

    <article className="p-3 my-3 bg-light rounded-3">
      <h1 className="text-center font-weight-bold my-3" style={{ fontSize: "0.1px" }}>
        PoHodová Devítka
        <Image src={logoBig} alt="PoHodová devítka" width={290} height={247} priority={true} />
      </h1>

      <h2 className="text-center mt-5">
        11. ročník | <span className="text-decoration-line-through ">15.9. 2024</span> 10.11. 2024 | fotbalový stadion ve Velké Bíteši
      </h2>

      <hr className="mb-5" />

      <div className="row">
        <div className="col-lg-6 offset-lg-3 mt-3">
          <div className="alert alert-danger" role="alert">
            <p>
              Z důvodu mizerného počasí a šílené předpovědi jsme, podobně jako jiné běžecké závody i
              jiné akce, nuceni přistoupit KE ZRUŠENÍ nedělní PoHodové devítky.
            </p>
            <p>Nicméně je stanoven náhradní termín 10. 11. 2024!</p>
            <p>
              Pro vás, kdo jste už zaplatili online. Startovné bude platit pro náhradní termín 10.
              11. nebo, pokud se nezúčastníte, budete mít pro ročník 2025 startovné zdarma.
            </p>
            <p>Děkujeme za pochopení</p>
          </div>
        </div>
      </div>

      <h3 className="h4 mt-5 text-center fw-bold">MULTIFUNKČNÍ TUNEL V DESIGNU PH9 V RÁMCI STARTOVNÍHO BALÍČKU</h3>
      <div className="d-flex justify-content-center mb-3">
        <Link href={imageUrl("tunel-2024.jpg")} target="_blank">
          <Image
            src={tunel}
            className="img-fluid img-thumbnail"
            alt="Tunel PoHodové devítky"
            width={236}
            height={236}
          />
        </Link>
      </div>

      <h3 className="h4 mt-5 text-center fw-bold">Lidový běh délky 2 km</h3>

      <p className="text-center">
        pro ty, kteří si netroufají na délku 9 km hlavního závodu, ale chtěli by si zazávodit
      </p>

      <h3 className="h4 mt-5 text-center fw-bold">SKÁKACÍ HRAD</h3>
      <p className="text-center">pro děti</p>

      <h3 className="h4 mt-5 text-center fw-bold">CENY PRO 3 NEJRYCHLEJŠÍ</h3>
      <p className="text-center">
        z každé věkové kategorie dle pohlaví a pro Bítešáky / Bítešačky
      </p>

      <h3 className="h4 mt-5 text-center fw-bold">TOMBOLA</h3>
      <p className="text-center">
        ze&nbsp;startovních čísel vylosujeme jedno, jehož nositel vyhraje poukaz na&nbsp;prodloužený
        běžecký víkend PoHodové devítky
      </p>

      <div className="row">
        <div className="col text-center pt-5 pb-3">
          <a className="btn btn-lg btn-danger" href={registraceUrl}>REGISTROVAT</a>
        </div>
      </div>

      <div className="table-responsive mt-5">
        <table className="table table-condensed table-bordered table-sm align-middle">
          <thead className="text-center">
            <tr>
              <th>ZÁVOD</th>
              <th>REGISTRACE</th>
              <th>STARTOVNÉ</th>
              <th>START</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td rowSpan={2}>
                <b>Dětské kategorie</b>
                <br />
                (ročníky 2012 a mladší)
              </td>
              <td>
                online (pouze do 6.11.)
              </td>
              <td className="text-center">zdarma</td>
              <td className="text-center" rowSpan={2}>
                <span className="fw-bold">od 11:00</span>
                <br />
                (od&nbsp;nejmladších)
              </td>
            </tr>
            <tr>
              <td>v den závodu na místě od&nbsp;9:30 do&nbsp;10:50</td>
              <td className="text-center">50&nbsp;Kč</td>
            </tr>

            <tr>
              <td rowSpan={2}>
              <span className="fw-bold">Lidový běh + dorost 2&nbsp;km</span>
                <br />
                (ročníky 2011 a starší)
              </td>
              <td>
                online (pouze do 6.11.)
              </td>
              <td className="text-center">zdarma</td>
              <td className="text-center fw-bold" rowSpan={2}>
                12:05
              </td>
            </tr>
            <tr>
              <td>v den závodu na místě od&nbsp;9:30 do&nbsp;10:50</td>
              <td className="text-center">50&nbsp;Kč</td>
            </tr>

            <tr>
              <td rowSpan={3}>
              <span className="fw-bold">hlavní závod 9 km</span>
                <br />
                (ročníky 2006 a starší)
              </td>
              <td>
                online do 31.8.
              </td>
              <td className="text-center">300&nbsp;Kč</td>
              <td className="text-center fw-bold" rowSpan={3}>12:00</td>
            </tr>
            <tr>
              <td>online 1.9.-6.11. (konec online registrace)</td>
              <td className="text-center">370&nbsp;Kč</td>
            </tr>
            <tr>
              <td>v den závodu na místě 9:30-11:30</td>
              <td className="text-center">450&nbsp;Kč</td>
            </tr>
          </tbody>
        </table>
      </div>

      <p className="px-4">
        Děti startují podle kategorií (od nejmladších) od 11 h. Nejprve startují děti, následují mladší
        žáci/žákyně a starší žáci/žákyně.
      </p>
      <p className="px-4">
        Start hlavního závodu ve 12 h.
      </p>
      <p className="px-4">
        Start lidového běhu bezprostředně po startu hlavního závodu. Součástí je závod
        dorosteneckých kategorií.
      </p>

      <Categories categories={data.categories} genders={data.genders} races={data.races} />

      <h3 className="h4 mt-5 mb-0">TRASA HLAVNÍHO ZÁVODU</h3>
      <Link
        href="https://mapy.cz/s/dugodajune"
        title="Trasa závodu"
        target="_blank"
        rel="noopener noreferrer"
      >
        <Image
          className="img-thumbnail my-3"
          src={mapa}
          alt="mapa závodu"
          width={1334}
          height={544}
        />
      </Link>
      <p>Přibližně 1/3 asfalt, 2/3 nezpevněné cesty.</p>

      <h3 className="h4 mt-5">DOPRAVA</h3>
      <p>
        Závod se běží za plného silničního provozu. Každý závodník běží na vlastní nebezpečí. Dle
        Nařízení Evropského parlamentu a Rady (EU) č. 2016/679 ze dne 27. dubna 2016 účastníci
        závodu zaplacením startovného souhlasí se zpracováním osobních údajů. Tato data nebudou
        organizátorem nijak zneužita.
      </p>

      <div className="row mt-5">
        <div className="col text-center">
          <a href={pageUrl()}>pohodovadevitka.cz</a>
        </div>
        <div className="col text-center">
          <a href={site.facebook_url}>Facebook</a>
        </div>
        <div className="col text-center">
          <a href={`mailto:${site.email}}`}>E-mail</a>
        </div>
      </div>
    </article>
  </Layout>
);

export default Propozice;

export const getStaticProps: GetStaticProps<Props> = async () => {
  const [raceEventHeaders, data] = await Promise.all([
    getRaceEventHeaders(),
    getRaceEventData(2024),
  ]);
  return {
    props: {
      data,
      raceEventHeaders,
    },
  };
};
