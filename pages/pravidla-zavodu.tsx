import Head from "next/head";

import GeneralMeta from "../components/general-meta";
import Layout from "../components/layout";
import OpenGraphArticle from "../components/open-graph-article";
import Title from "../components/title";
import { CommonPage, getCommonPageStaticProps } from "../lib/common-page";
import { pageUrl } from "../lib/fns";

export const path = "/pravidla-zavodu/";
export const title = "Pravidla závodu";

const PravidlaZavodu: CommonPage = ({ raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta canonical={pageUrl(path)} />
    <OpenGraphArticle title={title} url={pageUrl(path)} />

    <article className="p-3 my-3 bg-light rounded-3">
      <h1 className="pb-3 mb-0">Pravidla závodu</h1>
      <p>Závod se koná za plného silničního provozu.</p>
      <p>Každý závodník startuje na vlastní nebezpečí.</p>
      <p>Za závodníky mladší 18ti let zodpovídají jeho rodiče.</p>
      <p>Lze využít služby zdravotníka, který bude ve střehu na fotbalovém stadionu.</p>
      <p>Každý závodník je povinen zaplatit startovné.</p>
    </article>
  </Layout>
);

export default PravidlaZavodu;

export const getStaticProps = getCommonPageStaticProps;
