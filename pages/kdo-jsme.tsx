/* eslint-disable @next/next/no-img-element */
import { GetStaticProps } from "next";
import Link from "next/link";
import { FC } from "react";

import Layout from "../components/layout";
import { CommonPageProps } from "../lib/common-page";
import { getRaceEventHeaders } from "../lib/race-events";
import { pageUrl } from "../lib/fns";
import { getOrganizators, Organizator } from "../lib/organizators";
import GeneralMeta from "../components/general-meta";
import OpenGraphArticle from "../components/open-graph-article";
import Title from "../components/title";

export const path = "/kdo-jsme/";
export const title = "Kdo jsme?";

const pictureWidth = 200;

type Props = CommonPageProps<{ organizators: Organizator[] }>;

const KdoJsme: FC<Props> = ({ organizators, raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta canonical={pageUrl(path)} />
    <OpenGraphArticle title={title} url={pageUrl(path)} />

    <article className="p-3 my-3 bg-light rounded-3">
      <h1 className="pb-3 mb-0">{title}</h1>

      {organizators.map((x) => (
        <div className="card mt-3" key={x.id}>
          <div className="card-body">
            <Link
              href={x.picture.original}
              className="float-start me-3"
              title={x.name}
              target="_blank"
            >
              <picture>
                {x.picture.scaled.map(({ src, type }) => (
                  <source key={src} srcSet={src} type={type} />
                ))}
                <img
                  className="rounded img-fluid"
                  src={x.picture.scaled[0].src ?? x.picture.original}
                  alt={x.name}
                />
              </picture>
            </Link>
            <h2 id={x.id}>{x.name}</h2>
            <div dangerouslySetInnerHTML={{ __html: x.description }}></div>
          </div>
        </div>
      ))}
    </article>
  </Layout>
);

export default KdoJsme;

export const getStaticProps: GetStaticProps<Props> = () =>
  Promise.all([getOrganizators(), getRaceEventHeaders()]).then(
    ([organizators, raceEventHeaders]) => ({
      props: {
        organizators,
        raceEventHeaders,
      },
    }),
  );
