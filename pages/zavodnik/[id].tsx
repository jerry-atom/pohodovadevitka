import { FC, Fragment } from "react";
import { ParsedUrlQuery } from "querystring";
import { GetStaticPaths, GetStaticProps } from "next";
import { User } from "@prisma/client";
import Link from "next/link";

import Layout, { Props as LayoutProps } from "../../components/layout";
import { getRaceEventHeaders } from "../../lib/race-events";
import GeneralMeta from "../../components/general-meta";
import { pageUrl } from "../../lib/fns";
import Title from "../../components/title";
import { getUserProfile, getUserProfiles, UserProfile } from "../../lib/users";
import { site } from "../../lib/constants";

export const getPath = (id: User["id"]) => `/zavodnik/${id}/`;

interface Params extends ParsedUrlQuery {
  id: string;
}

type Props = LayoutProps & {
  userProfile: UserProfile;
};

const Trophy: FC<{ position: number | null }> = ({ position }) => {
  switch (position) {
    case 1:
      return <i className="bi-trophy-fill text-warning ms-2" role="img" aria-label="Trofej" />;
    case 2:
      return <i className="bi-trophy-fill text-danger ms-2" role="img" aria-label="Trofej" />;
    case 3:
      return <i className="bi-trophy-fill text-success ms-2" role="img" aria-label="Trofej" />;
    default:
      return null;
  }
};

const UserRaces: FC<Props> = ({ raceEventHeaders, userProfile }) => {
  return (
    <Layout raceEventHeaders={raceEventHeaders}>
      <Title title={userProfile.name} />
      <GeneralMeta canonical={pageUrl(getPath(userProfile.id))} />

      <div className="btn-toolbar d-flex justify-content-end d-print-none" role="toolbar">
        <Link
          href={`mailto:${site.author_email}?subject=Chyba ve výsledcích PH9&body=Zde popište chybu. Děkuji.`}
          className="btn btn-sm btn-warning ms-2"
          rel="noopener"
          target="_blank"
          title="Poslat e-mail autorovi"
        >
          <i className="bi-bug me-2" />
          Nahlásit chybu
        </Link>
      </div>

      <div className="p-3 my-3 bg-light rounded-3">
        <h1 className="pb-3 mb-0">{userProfile.name}</h1>

        <div className="row row-cols-1 row-cols-md-2 row-cols-md-3 g-3">
          {userProfile.results.map((result) => (
            <div className="col" key={result.id}>
              <div className="card">
                <div className="card-header">
                  Ročník <strong>{result.raceEventYear}</strong> / závod{" "}
                  <strong>{result.raceName}</strong>
                </div>
                <div className="card-body">
                  <p className="card-text">
                    Cílový čas: <strong>{result.time}</strong>
                    <br />
                    Kategorie: <strong>{result.categoryName}</strong>
                    <br />
                    Absolutní pořadí: <strong>{result.abs}</strong>
                    <Trophy position={result.abs} />
                    <br />
                    Pořadí v kategorii: <strong>{result.cat}</strong>
                    <Trophy position={result.cat} />
                    <br />
                    {result.specialCategories.map((x) => (
                      <Fragment key={x.raceEventId}>
                        <strong>{x.name}</strong>
                        <Trophy position={x.position} />
                        <br />
                      </Fragment>
                    ))}
                    Startovní číslo: <strong>{result.number}</strong>
                    <br />
                    Klub: {result.club ? <strong>{result.club}</strong> : "neuvedeno"}
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </Layout>
  );
};

export default UserRaces;

export const getStaticProps: GetStaticProps<Props, Params> = async (context) => {
  const id = parseInt(context.params!.id, 10);
  if (!isFinite(id)) {
    throw new Error("Invalid type parameter of ID");
  }
  const raceEventHeaders = await getRaceEventHeaders();
  const userProfile = await getUserProfile(id);
  return {
    props: {
      raceEventHeaders,
      userProfile,
    },
  };
};

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  const users = await getUserProfiles();

  return {
    paths: users.map(({ id }) => ({
      params: {
        id: String(id),
      },
    })),
    fallback: false,
  };
};
