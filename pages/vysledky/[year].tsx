import { FC, useReducer, useEffect, Fragment } from "react";
import { ParsedUrlQuery } from "querystring";
import { GetStaticPaths, GetStaticProps } from "next";
import { Category, Gender, Race } from "@prisma/client";

import Layout, { Props as LayoutProps } from "../../components/layout";
import { getRaceEventHeaders, getRaceEventData, RaceEventData } from "../../lib/race-events";
import Categories from "../../components/categories";
import PropositionDownloadButton from "../../components/proposition-download-button";
import PrintPageButton from "../../components/print-page-button";
import ResultsDownloadButton from "../../components/results-download-button";
import RaceSelect from "../../components/race-select";
import GenderSelect from "../../components/gender-select";
import CategorySelect from "../../components/category-select";
import ResultTable from "../../components/result-table";
import GeneralMeta from "../../components/general-meta";
import OpenGraphWebsite from "../../components/open-graph-website";
import { pageUrl } from "../../lib/fns";
import TwitterCardSummary from "../../components/twitter-card-summary";
import { getResultsByYear, Result } from "../../lib/results";
import Title from "../../components/title";
import Link from "next/link";
import { site } from "../../lib/constants";

export const getPath = (year: number) => `/vysledky/${year}/`;
export const getTitle = (year: number) => `Výsledky PoHodové devítky ${year}`;

interface Params extends ParsedUrlQuery {
  year: string;
}

type Action =
  | { type: "race"; race: Race["id"] | undefined }
  | { type: "gender"; gender: Gender["id"] | undefined }
  | { type: "category"; category: Category["id"] | undefined };

type State = {
  categories: Category[];
  genders: Gender[];
  races: Race[];
  results: Result[];
  selectedCategory: Category["id"] | undefined;
  selectedGender: Gender["id"] | undefined;
  selectedRace: Race["id"] | undefined;
};

type Props = LayoutProps & {
  data: RaceEventData;
  results: Result[];
};

const RaceEventLocal: FC<Props> = ({ data, raceEventHeaders, results }) => {
  function reducer(state: State, action: Action): State {
    switch (action.type) {
      case "category": {
        const selectedCategory = action.category;
        const categoryFilter = selectedCategory
          ? (x: Result) => x.categoryId === selectedCategory
          : (x: Result) => state.categories.some(({ id }) => id === x.categoryId);

        return {
          ...state,
          results: results.filter(categoryFilter),
          selectedCategory,
        };
      }
      case "gender": {
        const selectedGender = action.gender;

        let categories = data.categories;
        if (state.selectedRace) {
          categories = categories.filter((x) => x.raceId === state.selectedRace);
        }
        if (selectedGender) {
          categories = categories.filter((x) => x.genderId === action.gender);
        }

        const categoryIds = new Set(categories.map((x) => x.id));
        const selectedCategory =
          state.selectedCategory && categoryIds.has(state.selectedCategory)
            ? state.selectedCategory
            : undefined;

        const outResults =
          categoryIds.size > 0 ? results.filter((x) => categoryIds.has(x.categoryId)) : results;

        return {
          ...state,
          categories,
          results: outResults,
          selectedGender,
          selectedCategory,
        };
      }
      case "race": {
        const selectedRace = action.race;

        const categories = data.categories.filter((x) => x.raceId === selectedRace);
        const categoryIds = new Set(categories.map((x) => x.id));
        const selectedCategory = undefined;

        const genderIds = new Set(categories.map((x) => x.genderId));
        const genders = data.genders.filter((x) => genderIds.has(x.id));
        const selectedGender = undefined;

        const resultsOut =
          categoryIds.size > 0 ? results.filter((x) => categoryIds.has(x.categoryId)) : results;

        return {
          ...state,
          categories,
          genders,
          results: resultsOut,
          selectedCategory,
          selectedGender,
          selectedRace,
        };
      }
      default:
        return state;
    }
  }

  const [state, dispatch] = useReducer(reducer, {
    categories: data.categories,
    genders: data.genders,
    races: data.races,
    results,
    selectedCategory: undefined,
    selectedGender: undefined,
    selectedRace: undefined,
  });

  useEffect(() => {
    dispatch({ type: "race", race: data.races[0].id });
  }, [data]);
  const year = data.raceEvent.year;
  const title = getTitle(year);

  return (
    <Layout raceEventHeaders={raceEventHeaders}>
      <Title title={title} />
      <GeneralMeta canonical={pageUrl(getPath(year))} />
      <OpenGraphWebsite title={title} url={pageUrl(getPath(year))} />
      <TwitterCardSummary title={title} description={title} />

      <div className="btn-toolbar d-flex justify-content-end d-print-none" role="toolbar">
        <ResultsDownloadButton year={year} />
        <PrintPageButton />
        {data.raceEvent.printPropositionOriginal && <PropositionDownloadButton year={year} />}
        <Link
          href={`mailto:${site.author_email}?subject=Chyba ve výsledcích PH9&body=Zde popište chybu. Děkuji.`}
          className="btn btn-sm btn-warning ms-2"
          rel="noopener"
          target="_blank"
          title="Poslat e-mail autorovi"
        >
          <i className="bi-bug me-2" />
          Nahlásit chybu
        </Link>
      </div>

      <div className="p-3 my-3 bg-light rounded-3">
        <h1 className="pb-3 mb-0">{title}</h1>

        <div className="row g-3">
          <div className="col-md-4 mb-3">
            <RaceSelect
              onChange={(race) => dispatch({ type: "race", race })}
              races={state.races}
              selected={state.selectedRace}
            />
          </div>
          <div className="col-md-4 mb-3">
            <GenderSelect
              genders={state.genders}
              onChange={(gender) => dispatch({ type: "gender", gender })}
              selected={state.selectedGender}
            />
          </div>
          <div className="col-md-4 mb-3">
            <CategorySelect
              categories={state.categories}
              genders={state.genders}
              onChange={(category) => dispatch({ type: "category", category })}
              selected={state.selectedCategory}
            />
          </div>
        </div>

        <ResultTable results={state.results} />

        {data.specialCategories.length > 0 ? (
          <>
            <h2 className="my-3">Speciální kategorie</h2>
            <p>
              {data.specialCategories.map((specialCategory, index) => (
                <Fragment key={index}>
                  <b>{specialCategory.categoryName}</b>: {specialCategory.userName}
                  <br />
                </Fragment>
              ))}
            </p>
          </>
        ) : null}

        <Categories
          categories={data.categories}
          genders={data.genders}
          races={data.races}
          title="Vypsané kategorie"
        />
      </div>
    </Layout>
  );
};

const RaceEventRaceResult: FC<Props> = ({ data, raceEventHeaders }) => {
  const year = data.raceEvent.year;
  const title = getTitle(year);
  const raceId = data.raceEvent.viewType.split(":")[1];

  return (
    <Layout raceEventHeaders={raceEventHeaders}>
      <Title title={title} />
      <GeneralMeta canonical={pageUrl(getPath(year))} />
      <OpenGraphWebsite title={title} url={pageUrl(getPath(year))} />
      <TwitterCardSummary title={title} description={title} />
      <article className="p-3 my-3 bg-light rounded-3">
        <h1 className="text-center font-weight-bold my-3">{title}</h1>
        <div
          dangerouslySetInnerHTML={{
            __html: `
<div id="divRRPublish" class="RRPublish"></div>
<script src="https://my.raceresult.com/RRPublish/load.js.php?lang=cz"></script>
<script>
var rrp = new RRPublish(document.getElementById("divRRPublish"), ${raceId}, "results");
rrp.ShowTimerLogo = true;
rrp.ShowInfoText = false;
</script>`,
          }}
        />
      </article>
    </Layout>
  );
};

const RaceEventPage: FC<Props> = ({ data, ...rest }) => {
  if (data.raceEvent.viewType === "local") {
    return <RaceEventLocal data={data} {...rest} />;
  }
  return <RaceEventRaceResult data={data} {...rest} />;
};

export default RaceEventPage;

export const getStaticProps: GetStaticProps<Props, Params> = async (context) => {
  const year = parseInt(context.params!.year, 10);
  if (!isFinite(year)) {
    throw new Error("Invalid type parameter of year");
  }
  const [raceEventHeaders, data, results] = await Promise.all([
    getRaceEventHeaders(),
    getRaceEventData(year),
    getResultsByYear(year),
  ]);
  return {
    props: {
      data,
      raceEventHeaders,
      results,
    },
  };
};

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  const raceEventHeaders = await getRaceEventHeaders();

  return {
    paths: raceEventHeaders.map((raceEvent) => ({
      params: {
        year: String(raceEvent.year),
      },
    })),
    fallback: false,
  };
};
