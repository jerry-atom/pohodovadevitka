import { GetStaticProps } from "next";
import { FC } from "react";

import Layout from "../components/layout";
import { site } from "../lib/constants";
import { CommonPageProps, getCommonPageStaticProps } from "../lib/common-page";
import Title from "../components/title";

export const title = "Stránka nenalezena";

type Props = CommonPageProps;

const Custom404: FC<Props> = ({ raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />

    <div className="p-5 mb-4 bg-light rounded-3">
      <h1 className="display-5 fw-bold">{title}</h1>
      <p className="fs-4">
        Pokud veříte, že se jedná o chybu, prosíme, nahlaste nám ji na{" "}
        <a href={`mailto:${site.email}`}>e-mail závodu</a>.
      </p>
      <p>Děkujeme</p>
    </div>
  </Layout>
);

export default Custom404;

export const getStaticProps: GetStaticProps<Props> = getCommonPageStaticProps;
