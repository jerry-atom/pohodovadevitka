import { GetStaticProps } from "next";
import { FC } from "react";

import Layout from "../components/layout";
// import Countdown from "../components/countdown";
import { site } from "../lib/constants";
import { getRaceEventHeaders } from "../lib/race-events";
import { CommonPageProps } from "../lib/common-page";
import { getLastPosts, Post } from "../lib/posts";
import PostCard from "../components/post-card";
import { pageUrl, imageUrl } from "../lib/fns";
import Title from "../components/title";
import Carousel from "../components/carousel";
import OpenGraphWebsite from "../components/open-graph-website";
import GeneralMeta from "../components/general-meta";
import TwitterCardSummary from "../components/twitter-card-summary";

export const path = "/";
export const title = "Úvodní stránka";

type Props = CommonPageProps & {
  lastPosts: Post[];
};

const Home: FC<Props> = ({ lastPosts, raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta description={site.description} canonical={pageUrl()} />
    <OpenGraphWebsite
      title={site.title}
      url={pageUrl()}
      description={site.description}
      image={imageUrl("logo.jpg")}
    />
    <TwitterCardSummary
      title={site.description}
      description={site.description}
      image={imageUrl("logo.jpg")}
    />
    {/*
    <Countdown
      datetime="2023-09-17T11:00:00.000Z"
      registrationUrl="https://my.raceresult.com/251332/registration"
    />
    */}
    <Carousel />

    <h1 className="py-3 text-center text-bold">
      PoHodová devítka <small className="h4">běžecký závod Velké Bíteše</small>
    </h1>

    <p className="lead">
      Běžecký závod ve Velké Bíteši, který se pořádá v září, vždy po zdejších hodech. Hlavní trasa
      měří 9km, děti mohou běžet kratší trasy.
    </p>

    <div className="row row-cols-1 row-cols-md-2 g-4">
      {lastPosts.map((post, key) => (
        <div className="col" key={key}>
          <PostCard {...post} />
        </div>
      ))}
    </div>
  </Layout>
);

export default Home;

export const getStaticProps: GetStaticProps<Props> = () =>
  Promise.all([getLastPosts(4), getRaceEventHeaders()]).then(([lastPosts, raceEventHeaders]) => ({
    props: { lastPosts, raceEventHeaders },
  }));
