import Link from "next/link";

import GeneralMeta from "../components/general-meta";
import Layout from "../components/layout";
import OpenGraphArticle from "../components/open-graph-article";
import Title from "../components/title";
import { CommonPage, getCommonPageStaticProps } from "../lib/common-page";
import { fileUrl, pageUrl } from "../lib/fns";

export const path = "/trasa-zavodu/";
export const title = "Trasa závodu";

const TrasaZavodu: CommonPage = ({ raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta canonical={pageUrl(path)} />
    <OpenGraphArticle title="Trasa závodu" url={pageUrl(path)} />

    <div className="btn-toolbar d-flex justify-content-end d-print-none" role="toolbar">
      <Link
        href={fileUrl("Trasa-2018.gpx")}
        className="btn btn-md btn-success"
        target="_blank"
        rel="noopener"
        title="Stáhnout trasu ve formátu GPX"
      >
        <i className="bi-download me-2" />
        Stáhnout GPX
      </Link>
    </div>

    <article className="p-3 my-3 bg-light rounded-3">
      <h1 className="pb-3 mb-0">{title}</h1>

      <p className="lead">
        Ať už jste PoHodovou devítku běželi nebo ne, můžete se zde dozvědět spoustu užitečných
        informací. Například, kde se nachází start a cíl závodu, po jakém povrchu se poběží, na co
        si dát pozor, kde jsou záludné odbočky, a nebo, kde se nachází občerstvovací stanice.
      </p>

      <h2>Intinerář trasy</h2>

      <ul className="list-unstyled">
        <li>Po startu se zatáčí ostře doleva a běží se cca 1 km z kopce po asfaltu.</li>
        <li>Další 1 km následuje rozbitá asfaltová cesta.</li>
        <li>
          U Prostředního mlýna se nabíhá na lesní cestu, kterou se doběhne až ke Křovskému lomu.
        </li>
        <li>
          Odtud se pokračuje opět po asfaltu až po pravotočivou odbočku, kde se nabíhá na nejtěžší
          kopec závodu.
        </li>
        <li>Od startu až po tuto odbočku to je cca 4 km širokou cestou víceméně pořád z kopce.</li>
        <li>Po vyběhnutí kopce následuje široká lesní cesta a seběh zpět k Bílému potoku.</li>
        <li>
          Před Bílým potokem se točí doleva na nenápadnou lesní pěšinu. Jedná se o velice pěkný a
          technický úsek trvající cca 2 km.
        </li>
        <li>
          Před rybníkem Koupelna se točí doprava a pokračuje se na krátké, ale zato velice prudké
          stoupání.
        </li>
        <li>
          V seběhu z tohoto stoupání se točí ostře doprava. Zde se nachází občerstvovací stanice.
        </li>
        <li>
          Po odbočení doprava se nabíhá zpátky do lesa, následuje krátký prudký technický seběh a
          přeběh lávky.
        </li>
        <li>
          Trasa pokračuje na &ldquo;Letné&rdquo; velice pěknou lesní cestou a končí seběhem u
          Předního mlýna.
        </li>
        <li>
          Pak se točí doprava na most a posledním 1 km stoupáním po asfaltu se dobíhá do cíle.
        </li>
      </ul>
      <p>Jedná se o velice krásný a pestrý závod. Doufáme, že si ho společně s námi užijete.</p>
      <p>Váš tým PH9.</p>

      <div className="ratio ratio-16x9">
        <iframe
          src="https://www.youtube.com/embed/YEHV-4j98L8"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen={true}
          frameBorder={0}
        ></iframe>
      </div>
    </article>
  </Layout>
);

export default TrasaZavodu;

export const getStaticProps = getCommonPageStaticProps;
