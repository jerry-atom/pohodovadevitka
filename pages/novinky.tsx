import { parseISO, format } from "date-fns";
import { cs } from "date-fns/locale";
import { GetStaticProps } from "next";
import Link from "next/link";
import { FC } from "react";

import Layout from "../components/layout";
import { CommonPageProps } from "../lib/common-page";
import { pageUrl } from "../lib/fns";
import { getAllPosts, Post } from "../lib/posts";
import { getRaceEventHeaders } from "../lib/race-events";
import Title from "../components/title";
import GeneralMeta from "../components/general-meta";

const convertIsoDate = (isoDate: string) => {
  const dateObject = parseISO(isoDate);
  return format(dateObject, "d.M.y", { locale: cs });
};

export const path = "/novinky/";
export const title = "Novinky";

type Props = CommonPageProps & {
  posts: Post[];
};

const Novinky: FC<Props> = ({ posts, raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={title} />
    <GeneralMeta canonical={pageUrl(path)} />

    <article className="p-3 my-3 bg-light rounded-3">
      <h1 className="pb-3 mb-0">{title}</h1>

      <div className="list-group">
        {posts.map(({ createdAt, title, path }, key) => (
          <Link
            href={pageUrl(path)}
            key={key}
            className="list-group-item list-group-item-action flex-column align-items-start"
          >
            <div className="d-flex w-100 justify-content-between">
              <h5 className="mb-1">{title}</h5>
              <time dateTime={createdAt}>{convertIsoDate(createdAt)}</time>
            </div>
          </Link>
        ))}
      </div>
    </article>
  </Layout>
);

export default Novinky;

export const getStaticProps: GetStaticProps<Props> = () =>
  Promise.all([getAllPosts(), getRaceEventHeaders()]).then(([posts, raceEventHeaders]) => ({
    props: { posts, raceEventHeaders },
  }));
