import { FC } from "react";
import Link from "next/link";
import { ParsedUrlQuery } from "querystring";
import { GetStaticPaths, GetStaticProps } from "next";

import Layout, { Props as LayoutProps } from "../components/layout";
import { getRaceEventHeaders } from "../lib/race-events";
import { getAllPosts, getPostByPath, Post } from "../lib/posts";
import { pageUrl, convertDateTime } from "../lib/fns";
import GeneralMeta from "../components/general-meta";
import OpenGraphArticle from "../components/open-graph-article";
import TwitterCardSummary from "../components/twitter-card-summary";
import Title from "../components/title";

interface Params extends ParsedUrlQuery {
  slug: string[];
}

type Props = LayoutProps & {
  post: Post;
};

const PostPage: FC<Props> = ({ post, raceEventHeaders }) => (
  <Layout raceEventHeaders={raceEventHeaders}>
    <Title title={post.title} />
    <GeneralMeta
      description={post.description}
      canonical={pageUrl(post.path)}
      nextPath={post.nextPath && pageUrl(post.nextPath)}
      prevPath={post.prevPath && pageUrl(post.prevPath)}
    />
    <OpenGraphArticle
      title={post.title}
      url={pageUrl(post.path)}
      description={post.description}
      publishedTime={post.createdAt}
    />
    <TwitterCardSummary title={post.title} description={post.description} />

    {(post.prevPath || post.nextPath) && (
      <div className="btn-toolbar d-flex justify-content-end d-print-none" role="toolbar">
        {post.prevPath && (
          <Link href={pageUrl(post.prevPath)} className="btn btn-sm btn-success ms-2">
            <i className="bi-arrow-left me-2" /> Předchozí novinka
          </Link>
        )}
        {post.nextPath && (
          <Link href={pageUrl(post.nextPath)} className="btn btn-sm btn-success ms-2">
            <i className="bi-arrow-right me-2" /> Následující novinka
          </Link>
        )}
      </div>
    )}

    <article className="p-3 my-3 bg-light rounded-3">
      <div className="text-end">
        Publikováno <time dateTime={post.createdAt}>{convertDateTime(post.createdAt)}</time>
      </div>

      <h1 className="pb-3 mb-0">{post.title}</h1>

      <div id="article" dangerouslySetInnerHTML={{ __html: post.content }} />
    </article>
  </Layout>
);

export default PostPage;

export const getStaticProps: GetStaticProps<Props, Params> = async ({ params }) => {
  return Promise.all([getPostByPath(params!.slug.join("/")), getRaceEventHeaders()]).then(
    ([post, raceEventHeaders]) => ({
      props: {
        post,
        raceEventHeaders,
      },
    }),
  );
};

export const getStaticPaths: GetStaticPaths<Params> = async () => {
  return {
    paths: (await getAllPosts()).map((x) => ({
      params: {
        slug: x.path.split("/"),
      },
    })),
    fallback: false,
  };
};
