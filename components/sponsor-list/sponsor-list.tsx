import { FC } from "react";
import Sponsor from "./sponsor";

const DEFAULT_AREA = 11_000;
const CLOUDINARY_CLOUD_NAME = "dk1shqmyz";

export function calcEntry(pictureId: string, width: number, height: number, area = DEFAULT_AREA) {
  const scale = Math.sqrt(area / width / height);
  const w = Math.ceil(scale * width);
  return {
    logo: `https://res.cloudinary.com/${CLOUDINARY_CLOUD_NAME}/image/upload/c_scale,w_${w}/q_auto:eco/f_webp/ph9/sponsor/${pictureId}.webp`,
    height: Math.round(100 * scale * height) / 100,
    width: Math.round(100 * scale * width) / 100,
  };
}

const SponsorList: FC<{}> = () => (
  <div id="menu-sponsors">
    <div className="text-secondary">Hlavní sponzor:</div>
    <div className="d-flex justify-content-center align-items-center flex-wrap gap-2 py-2 sponsors">
      <a href="https://kviti-jinak.cz/" rel="noreferer" style={{ fontSize: 0 }}>
        Kvítí jinak
      </a>
      <Sponsor
        title="Velká Bíteš"
        href="https://www.vbites.cz"
        {...calcEntry("vbites", 317, 65, 22_000)}
      />
    </div>
    <div className="text-secondary">Ostatní sponzoři:</div>
    <div className="d-flex justify-content-center align-items-center flex-wrap gap-2 py-2 sponsors">
      <Sponsor
        title="FC PBS Velká Bíteš z.s."
        href="https://fcvelkabites.cz/"
        {...calcEntry("fc-pbs-vb", 206, 206)}
      />
      <Sponsor
        title="DálRunDál z.s"
        href="https://www.dalrundal.cz/"
        {...calcEntry("dalrundal", 389, 309)}
      />
      <Sponsor
        title="MAFLEX-CZ s.r.o."
        href="https://www.maflex-cz.cz/"
        {...calcEntry("maflex", 230, 60)}
      />
      <Sponsor
        title="BRUSIVO spol. s r.o."
        href="https://brusivo.eu/"
        {...calcEntry("brusivo", 286, 123)}
      />
      <Sponsor
        title="Fit-Plán s Mari"
        href="https://linktr.ee/fitmama_mary"
        {...calcEntry("fit-plan", 303, 419)}
      />
      <Sponsor
        title="TurboCar, s.r.o."
        href="https://www.turbocar.cz"
        {...calcEntry("turbocar", 576, 68)}
      />
      <Sponsor
        title="Skin Art - Tetování Velká Bíteš"
        href="https://skinart.cz/"
        {...calcEntry("skinart", 134, 134)}
      />
      <Sponsor
        title="BÍTEŠSKÁ DOPRAVNÍ SPOLEČNOST, spol. s r.o."
        href="https://www.bds-vb.cz/cs/"
        {...calcEntry("bds", 265, 38)}
      />
      <Sponsor
        title="Blank Súčková - textilní výroba"
        href="https://www.fler.cz/essence-b"
        {...calcEntry("fler-suckova", 208, 56)}
      />
      <Sponsor
        title="Fitcentrum OLMPIA Tišnov"
        href="http://www.posilovna-tisnov.cz"
        {...calcEntry("fitness-tisnov", 200, 111)}
      />
      <Sponsor
        title="KARGOMA, s.r.o."
        href="https://www.facebook.com/kargomavb"
        {...calcEntry("kargoma", 221, 49)}
      />
      <Sponsor
        title="AG FOODS Group, a.s."
        href="https://www.agfoods.eu"
        {...calcEntry("agfoods", 215, 51)}
      />
      <Sponsor
        title="Alfa Agro Brno, s.r.o."
        href="http://www.alfaagro.cz"
        {...calcEntry("alfaagro", 193, 56)}
      />
      <Sponsor
        title="BIBUS METALS s.r.o."
        href="https://www.bibusmetals.cz"
        {...calcEntry("bibusmetals", 263, 42)}
      />
      <Sponsor
        title="První brněnská strojírna Velká Bíteš, a.s."
        href="https://www.pbs.cz/cz/"
        {...calcEntry("pbsvb", 248, 44)}
      />
      <Sponsor
        title="Promat servis, s.r.o."
        href="http://www.promatservis.cz"
        {...calcEntry("promatservis", 197, 56)}
      />
      <Sponsor
        title="Primapol-Metal-Spot, s.r.o."
        href="https://www.primapol.cz"
        {...calcEntry("primapol", 235, 47)}
      />
      <Sponsor
        title="Informační centrum - Klub kultury - Velká Bíteš"
        href="https://www.vbites.cz/zivot-ve-meste/2017-03-28-07-19-31/informacni-centrum-a-klub-kultury"
        {...calcEntry("ickk", 171, 64)}
      />
      <Sponsor
        title="MT-Metal Trade, s.r.o."
        href="https://www.mtmetal.com"
        {...calcEntry("mtmetal", 105, 104)}
      />
      <Sponsor
        title="Zámečnictví Němec, s.r.o."
        href="https://www.zamecnictvinemec.cz"
        {...calcEntry("zamecnictvinemec", 212, 52)}
      />
      <Sponsor
        title="Dynamic Metals Ltd"
        href="https://www.dynamicmetalsltd.cz"
        {...calcEntry("dynamicmetals", 289, 38)}
      />
      <Sponsor
        title="UZENÁŘSTVÍ A LAHŮDKY SLÁMA s.r.o."
        href="https://www.slamamilan.cz"
        {...calcEntry("slamamilan", 124, 89)}
      />
      <Sponsor
        title="Kvítí jinak - květinářství v Přerově"
        href="https://kviti-jinak.cz"
        {...calcEntry("kviti-jinak", 600, 169)}
      />
    </div>
  </div>
);

export default SponsorList;
