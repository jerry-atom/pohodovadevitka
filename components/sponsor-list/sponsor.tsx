import { FC } from "react";
import Image, { StaticImageData } from "next/image";
import Link from "next/link";

export type Props = {
  height: number;
  href: string;
  logo: StaticImageData | string;
  title: string;
  width: number;
};

const Sponsor: FC<Props> = ({ height, href, logo, title, width }) => (
  <Link href={href} title={title} target="_blank" rel="noopener noreferrer" className="m-2">
    <Image
      alt={`Sponzor závodu: ${title}`}
      src={logo}
      height={height}
      width={width}
      loading="lazy"
    />
  </Link>
);

export default Sponsor;
