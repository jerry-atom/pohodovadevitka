import Head from "next/head";
import { FC } from "react";

type Props = {
  description?: string;
  image?: string;
  title: string;
  url: string;
};

const OpenGraphWebsite: FC<Props> = ({ description, image, title, url }) => (
  <Head>
    <meta property="og:type" content="website" />
    <meta property="og:title" content={title} />
    <meta property="og:url" content={url} />
    {description && <meta property="og:description" content={description} />}
    {image && <meta property="og:image" content={image} />}
  </Head>
);

export default OpenGraphWebsite;
