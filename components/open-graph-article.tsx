import Head from "next/head";
import { FC } from "react";

type Props = {
  description?: string;
  image?: string;
  publishedTime?: string;
  title: string;
  url: string;
};

const OpenGraphArticle: FC<Props> = ({ description, image, publishedTime, title, url }) => (
  <Head>
    <meta property="og:type" content="article" />
    <meta property="og:title" content={title} />
    <meta property="og:url" content={url} />
    {description && <meta property="og:description" content={description} />}
    {publishedTime && <meta property="article:published_time" content={publishedTime} />}
    {image && <meta property="og:image" content={image} />}
  </Head>
);

export default OpenGraphArticle;
