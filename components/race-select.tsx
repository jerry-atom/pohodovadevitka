import { Race } from "@prisma/client";
import { FC } from "react";

type Props = {
  htmlId?: string;
  onChange: (value: number) => void;
  races: Race[];
  selected?: number;
};

const RaceSelect: FC<Props> = ({ htmlId = "race", onChange, races, selected }) => (
  <>
    <label htmlFor={htmlId} className="form-label">
      Závod:
    </label>
    <select
      className="form-select"
      defaultValue={selected}
      id={htmlId}
      onChange={(event) => onChange(Number(event.target.value))}
    >
      {races.map((x) => (
        <option key={x.id} value={x.id}>
          {x.name}
        </option>
      ))}
    </select>
  </>
);

export default RaceSelect;
