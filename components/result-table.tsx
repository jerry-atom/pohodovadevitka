import Link from "next/link";
import { FC } from "react";
import { pageUrl } from "../lib/fns";
import { Result } from "../lib/results";
import { getPath } from "../pages/zavodnik/[id]";

const ResultProps = ({ props }: { props: string }) => {
  try {
    const data = JSON.parse(props);
    if (
      data == null ||
      typeof data != "object" ||
      !("video" in data) ||
      typeof data.video !== "string"
    ) {
      return null;
    }
    return (
      <Link href={data.video} target="_blank" rel="noreferrer nofollow noopener">
        <i className="bi bi-play-btn h4 ms-2 text-black"></i>
      </Link>
    );
  } catch (error) {
    return null;
  }
};

const Time: FC<{ time: string }> = ({ time }) => {
  switch (time) {
    case "DNF":
      return (
        <abbr data-bs-toggle="tooltip" title="nedokončil(a) závod">
          DNF
        </abbr>
      );
    case "DNS":
      return (
        <abbr data-bs-toggle="tooltip" title="nevystartoval(a)">
          DNS
        </abbr>
      );
    default:
      return <strong>{time}</strong>;
  }
};

type Props = {
  results: Result[];
};

const ResultTable: FC<Props> = ({ results }) => (
  <div className="table-responsive">
    <table className="table table-sm table-hover results">
      <thead>
        <tr>
          <th>
            <abbr data-bs-toggle="tooltip" title="absolutní pořadí">
              poř.
            </abbr>
          </th>
          <th>
            <abbr data-bs-toggle="tooltip" title="kategorie">
              kat.
            </abbr>
          </th>
          <th>
            <abbr data-bs-toggle="tooltip" title="pořadí v kategorii">
              poř.<>&nbsp;</>k.
            </abbr>
          </th>
          <th>
            <abbr data-bs-toggle="tooltip" title="startovní číslo">
              s.<>&nbsp;</>č.
            </abbr>
          </th>
          <th className="name">jméno</th>
          <th className="number">čas</th>
        </tr>
      </thead>
      <tbody>
        {results.map((x) => (
          <tr key={x.id}>
            <td>{x.abs}</td>
            <td>{x.Category.abbreviation}</td>
            <td>{x.cat}</td>
            <td>{x.number}</td>
            <td className="name">
              <b>
                {x.userId ? (
                  <Link href={pageUrl(getPath(x.userId))}>{x.user.name}</Link>
                ) : (
                  x.user.name
                )}
              </b>{" "}
              {x.club}
              {x.props && <ResultProps props={x.props} />}
            </td>
            <td className="number font-monospace">{x.time && <Time time={x.time} />}</td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

export default ResultTable;
