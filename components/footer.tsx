import Link from "next/link";
import { FC } from "react";
import { site } from "../lib/constants";
import { pageUrl } from "../lib/fns";
import SponsorList from "./sponsor-list";

const Footer: FC = () => (
  <footer className="d-print-none">
    <div className="container-fluid">
      <SponsorList />

      <ul className="nav justify-content-center pb-3 icons">
        <li className="nav-item">
          <Link href={pageUrl()} className="nav-link text-secondary" title="Oficiální web závodu">
            <i className="bi-house-fill h4" role="img" aria-label="Web" />
            <span className="visually-hidden">Oficiální web závodu</span>
          </Link>
        </li>
        <li className="nav-item">
          <Link
            href={site.facebook_url}
            className="nav-link text-secondary"
            title="Facebook závodu"
          >
            <i className="bi-facebook h4" role="img" aria-label="Facebook" />
            <span className="visually-hidden">Facebook závodu</span>
          </Link>
        </li>
        <li className="nav-item">
          <Link href={site.twitter_url} className="nav-link text-secondary" title="Twitter závodu">
            <i className="bi-twitter h4" role="img" aria-label="Twitter" />
            <span className="visually-hidden">Twitter závodu</span>
          </Link>
        </li>
        <li className="nav-item">
          <Link
            href={`mailto:${site.email}`}
            className="nav-link text-secondary"
            title="E-mail závodu"
          >
            <i className="bi-envelope-fill h4" role="img" aria-label="E-mail" />
            <span className="visually-hidden">E-mail závodu</span>
          </Link>
        </li>
        <li className="nav-item">
          <Link
            href="https://gitlab.com/jerry-atom/pohodovadevitka"
            className="nav-link text-secondary"
            title="Zdrojový kód"
          >
            <i className="bi-git h4" role="img" aria-label="Zdrojový kód" />
            <span className="visually-hidden">Zdrojový kód</span>
          </Link>
        </li>
      </ul>
      <p className="text-center">
        &copy; Zdeněk Kundera 2014 - {new Date().getFullYear()}
        {" | "}
        <Link
          href={`mailto:${site.author_email}?subject=Chyba na PH9&body=Zde popište chybu. Děkuji.`}
          title="Kontakt na autora webu"
          target="_blank"
        >
          Nahlásit chybu
        </Link>
      </p>
    </div>
  </footer>
);

export default Footer;
