import { FC, PropsWithChildren, useEffect } from "react";
import Header, { Props as HeaderProps } from "./header";
import Footer from "./footer";

export type Props = PropsWithChildren<HeaderProps>;

const Layout: FC<Props> = ({ children, raceEventHeaders }) => {
  useEffect(() => {
    import("bootstrap/dist/js/bootstrap" as any);
  }, []);

  return (
    <>
      <Header raceEventHeaders={raceEventHeaders} />
      <main>
        <div className="container py-3">{children}</div>
      </main>
      <Footer />
    </>
  );
};

export default Layout;
