import { FC } from "react";
import Link from "next/link";
import { fileUrl } from "../lib/fns";

type Props = {
  year: number;
};

const ResultsDownloadButton: FC<Props> = ({ year }) => (
  <Link
    href={fileUrl(`Vysledky-${year}.pdf`)}
    className="btn btn-sm btn-success ms-2"
    rel="noopener"
    target="_blank"
    title="Stáhne originální soubor s výsledky od poskytovatele časomíry"
  >
    <i className="bi-download me-2" />
    Originál
  </Link>
);

export default ResultsDownloadButton;
