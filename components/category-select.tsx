import { Category, Gender } from "@prisma/client";
import { FC } from "react";

type Props = {
  categories: Category[];
  genders: Gender[];
  htmlId?: string;
  onChange: (value: number | undefined) => void;
  selected?: number;
};

const CategorySelect: FC<Props> = ({
  categories,
  genders,
  htmlId = "category",
  onChange,
  selected,
}) => (
  <>
    <label htmlFor={htmlId} className="form-label">
      Kategorie:
    </label>
    <select
      className="form-select"
      id={htmlId}
      onChange={(event) =>
        onChange(
          categories.some((x) => x.id === Number(event.target.value))
            ? Number(event.target.value)
            : undefined,
        )
      }
      value={selected ?? ""}
    >
      <option value="">- všechny -</option>
      {genders.map((x) => (
        <optgroup key={x.id} label={x.name}>
          {categories
            .filter((y) => y.genderId === x.id)
            .map((y) => (
              <option key={y.id} value={y.id}>
                {y.name}
              </option>
            ))}
        </optgroup>
      ))}
    </select>
  </>
);

export default CategorySelect;
