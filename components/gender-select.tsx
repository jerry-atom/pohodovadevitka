import { Gender } from "@prisma/client";
import { FC } from "react";

type Props = {
  genders: Gender[];
  htmlId?: string;
  onChange: (value: number | undefined) => void;
  selected?: number;
};

const GenderSelect: FC<Props> = ({ genders, htmlId = "gender", onChange, selected }) => (
  <>
    <label htmlFor={htmlId} className="form-label">
      Pohlaví:
    </label>
    <select
      className="form-select"
      id={htmlId}
      onChange={(event) =>
        onChange(
          genders.some((x) => x.id === Number(event.target.value))
            ? Number(event.target.value)
            : undefined,
        )
      }
      value={selected ?? ""}
    >
      <option value="">- všichni -</option>
      {genders.map((x) => (
        <option key={x.id} value={x.id}>
          {x.name}
        </option>
      ))}
    </select>
  </>
);

export default GenderSelect;
