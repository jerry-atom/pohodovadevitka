import { FC } from "react";

const PrintPageButton: FC = () => (
  <button
    className="btn btn-sm btn-success ms-2"
    onClick={() => window.print()}
    title="Zobrazit stránku k tisku"
  >
    <i className="bi-printer me-2" />
    Tisk
  </button>
);

export default PrintPageButton;
