import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import Categories, { buildCategoriesTable } from "../categories";
import { Category } from "../../lib/categories";
import { Gender } from "../../lib/genders";
import { Race } from "../../lib/races";

const genders: Gender[] = [
  { id: "A", name: "Lidový běh" },
  { id: "F", name: "Ženy" },
  { id: "M", name: "Muži" },
];
const races: Race[] = [
  { id: "9km", name: "Hlavní závod 9 km", distance: 9000 },
  { id: "1200m", name: "1.2 km", distance: 1200 },
  { id: "500m", name: "500 m", distance: 500 },
  { id: "400m", name: "400 m", distance: 400 },
  { id: "300m", name: "300 m", distance: 300 },
];
const categories: Category[] = [
  {
    id: "MA",
    abbreviation: "MA",
    gender: "M",
    race: "9km",
    name: "Muži „A“",
    yearFrom: 1982,
    yearTo: 2003,
  },
  {
    id: "MB",
    abbreviation: "MB",
    gender: "M",
    race: "9km",
    name: "Muži „B“",
    yearFrom: 1972,
    yearTo: 1981,
  },
  {
    id: "MC",
    abbreviation: "MC",
    gender: "M",
    race: "9km",
    name: "Muži „C“",
    yearFrom: 1962,
    yearTo: 1971,
  },
  {
    id: "MD",
    abbreviation: "MD",
    gender: "M",
    race: "9km",
    name: "Muži „D“",
    yearFrom: null,
    yearTo: 1961,
  },
  {
    id: "FA",
    abbreviation: "FA",
    gender: "F",
    race: "9km",
    name: "Ženy „A“",
    yearFrom: 1987,
    yearTo: 2003,
  },
  {
    id: "FB",
    abbreviation: "FB",
    gender: "F",
    race: "9km",
    name: "Ženy „B“",
    yearFrom: 1977,
    yearTo: 1986,
  },
  {
    id: "FC",
    abbreviation: "FC",
    gender: "F",
    race: "9km",
    name: "Ženy „C“",
    yearFrom: null,
    yearTo: 1976,
  },
  {
    id: "L",
    abbreviation: "L",
    gender: "A",
    race: "1200m",
    name: "Lidový běh",
    yearFrom: null,
    yearTo: 2003,
  },
  {
    id: "DM",
    abbreviation: "DM",
    gender: "M",
    race: "1200m",
    name: "Dorostenci",
    yearFrom: 2004,
    yearTo: 2008,
  },
  {
    id: "DF",
    abbreviation: "DF",
    gender: "F",
    race: "1200m",
    name: "Dorostenky",
    yearFrom: 2004,
    yearTo: 2008,
  },
  {
    id: "JM1",
    abbreviation: "JM1",
    gender: "M",
    race: "500m",
    name: "Starší žáci",
    yearFrom: 2009,
    yearTo: 2011,
  },
  {
    id: "JF1",
    abbreviation: "JF1",
    gender: "F",
    race: "500m",
    name: "Starší žákyně",
    yearFrom: 2009,
    yearTo: 2011,
  },
  {
    id: "JM2",
    abbreviation: "JM2",
    gender: "M",
    race: "400m",
    name: "Mladší žáci",
    yearFrom: 2012,
    yearTo: 2014,
  },
  {
    id: "JF2",
    abbreviation: "JF2",
    gender: "F",
    race: "400m",
    name: "Mladší žákyně",
    yearFrom: 2012,
    yearTo: 2014,
  },
  {
    id: "CM",
    abbreviation: "CM",
    gender: "M",
    race: "300m",
    name: "Děti",
    yearFrom: 2015,
    yearTo: null,
  },
  {
    id: "CF",
    abbreviation: "CF",
    gender: "F",
    race: "300m",
    name: "Děti",
    yearFrom: 2015,
    yearTo: null,
  },
];

describe("buildCategoriesTable", () => {
  it("returns the table structure", () => {
    const result = buildCategoriesTable(categories);

    expect(result).toEqual({
      columns: [genders[2].id, genders[1].id, genders[0].id],
      rows: [
        {
          categories: {
            [genders[0].id]: null,
            [genders[1].id]: categories[4].id,
            [genders[2].id]: categories[0].id,
          },
          race: { value: races[0].id, rowSpan: 4 },
        },
        {
          categories: {
            [genders[0].id]: null,
            [genders[1].id]: categories[5].id,
            [genders[2].id]: categories[1].id,
          },
        },
        {
          categories: {
            [genders[0].id]: null,
            [genders[1].id]: categories[6].id,
            [genders[2].id]: categories[2].id,
          },
        },
        {
          categories: {
            [genders[0].id]: null,
            [genders[1].id]: null,
            [genders[2].id]: categories[3].id,
          },
        },
        {
          categories: {
            [genders[0].id]: categories[7].id,
            [genders[1].id]: categories[9].id,
            [genders[2].id]: categories[8].id,
          },
          race: { value: races[1].id, rowSpan: 1 },
        },
        {
          categories: {
            [genders[0].id]: null,
            [genders[1].id]: categories[11].id,
            [genders[2].id]: categories[10].id,
          },
          race: { value: races[2].id, rowSpan: 1 },
        },
        {
          categories: {
            [genders[0].id]: null,
            [genders[1].id]: categories[13].id,
            [genders[2].id]: categories[12].id,
          },
          race: { value: races[3].id, rowSpan: 1 },
        },
        {
          categories: {
            [genders[0].id]: null,
            [genders[1].id]: categories[15].id,
            [genders[2].id]: categories[14].id,
          },
          race: { value: races[4].id, rowSpan: 1 },
        },
      ],
    });
  });
});

describe("Categories", () => {
  it("renders the table", () => {
    const result = render(
      <Categories
        categories={categories}
        genders={genders}
        races={races}
        title="Výsledky PoHodové devítky 2021"
      />,
    );

    expect(result.baseElement).toMatchSnapshot();
  });
});
