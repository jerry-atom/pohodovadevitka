import Link from "next/link";
import { FC, useEffect, useRef, useState } from "react";

type Props = {
  datetime: string; // "2021-09-19T11:30:00"
  registrationUrl: string; // "https://ptime.cz/registrace?id=61"
};

const Countdown: FC<Props> = ({ datetime, registrationUrl }) => {
  const [message, setMessage] = useState("");
  const timer = useRef<ReturnType<typeof setInterval> | null>();
  useEffect(() => {
    const start = Date.parse(datetime);
    if (start - Date.now() < 0) {
      setMessage("Start již proběhl");
    } else {
      setMessage("Startujeme již za...");
      timer.current = setInterval(() => {
        const t = (start - Date.now()) / 1000;
        if (t > 0) {
          const M = 60;
          const H = 60 * M;
          const D = 24 * H;

          const d = Math.floor(t / D);
          const sd = d * D;
          const h = Math.floor((t - sd) / H);
          const sh = h * H;
          const m = Math.floor((t - sd - sh) / M);
          const sm = m * M;
          const s = Math.floor(t - sd - sh - sm);

          const time = [d, " dní ", h, "h ", m, "m ", s, "s"];

          if (d === 1) {
            time[1] = " den ";
          } else if (d < 5) {
            time[1] = " dny ";
          }

          setMessage(`Startujeme již za ${time.join("")}`);
        } else {
          setMessage("Start již proběhl");
          if (timer.current != null) {
            clearInterval(timer.current);
            timer.current = null;
          }
        }
      }, 1000);
    }
  }, [datetime]);

  return (
    <div className="card mb-3">
      <div className="card-body text-center">
        <p className="card-text h3">{message}</p>
        {/*
        <p className="cart-text text-center h5">
          Online registrace skončila. Další registrace bude probíhat na místě mezi 12:00 a 13:15.
        </p>
        */}
        <Link
          href={registrationUrl}
          className="btn btn-lg btn-danger mt-3"
          role="button"
          rel="noopener noreferrer"
          target="_blank"
        >
          Registrovat
        </Link>
      </div>
    </div>
  );
};

export default Countdown;
