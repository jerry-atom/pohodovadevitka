import { FC, PropsWithChildren } from "react";
import cls from "classnames";
import Link from "next/link";
import { useRouter } from "next/router";

import { site } from "../lib/constants";
import { path as registraceUrl } from "../pages/registrace";
import { path as startovkaUrl } from "../pages/startovka";
import { path as propoziceUrl } from "../pages/propozice";
import { path as trasaZavoduUrl } from "../pages/trasa-zavodu";
import { path as kdoJsmeUrl } from "../pages/kdo-jsme";
import { path as jakToCeleVznikloUrl } from "../pages/jak-to-cele-vzniklo";
import { path as novinkyUrl } from "../pages/novinky";
// import { path as zpracovaniOsonichUdajuUrl } from "../pages/zpracovani-osobnich-udaju";
// import { path as pravidlaZavoduUrl } from "../pages/pravidla-zavodu";
import { RaceEventHeader } from "../lib/race-events";
import { getPath, getTitle } from "../pages/vysledky/[year]";

const NavItem: FC<PropsWithChildren<{ href: string }>> = ({ children, href }) => {
  const router = useRouter();

  return (
    <li className={cls("nav-item", { active: router.pathname === href })}>
      <Link href={href} className="nav-link">
        {children}
      </Link>
    </li>
  );
};

const DropdownItem: FC<PropsWithChildren<{ href: string; viewType?: string }>> = ({
  children,
  href,
  viewType,
}) => {
  const router = useRouter();

  return (
    <li className={cls("dropdown-item", { active: router.pathname === href })}>
      {!viewType || viewType === "local" ? (
        <Link href={href} className="nav-link">
          {children}
        </Link>
      ) : (
        <a href={href} className="nav-link">
          {children}
        </a>
      )}
    </li>
  );
};

export type Props = {
  raceEventHeaders: RaceEventHeader[];
};

const TopMenu: FC<Props> = ({ raceEventHeaders }) => {
  const router = useRouter();

  return (
    <ul className="navbar-nav justify-content-end" style={{ zIndex: 10000 }}>
      <li className={cls("nav-item", { active: router.pathname === registraceUrl })}>
        <a className="nav-link" href={registraceUrl}>
          Registrace
        </a>
      </li>
      <li className={cls("nav-item", { active: router.pathname === startovkaUrl })}>
        <a className="nav-link" href={startovkaUrl}>
          Startovka
        </a>
      </li>
      <NavItem href={propoziceUrl}>Propozice</NavItem>

      <li className="nav-item">
        <Link
          href={site.gallery_url}
          className="nav-link"
          target="_blank"
          rel="noopener noreferrer"
        >
          Fotogalerie
        </Link>
      </li>

      <li className="nav-item dropdown">
        <Link
          href="#"
          className="nav-link dropdown-toggle"
          data-bs-toggle="dropdown"
          role="button"
          aria-haspopup="true"
          aria-expanded="false"
          id="fotogalerie-top-dropdown"
        >
          Fotogalerie
        </Link>
        <ul className="dropdown-menu" role="menu" aria-labelledby="fotogalerie-top-dropdown">
          <DropdownItem href="https://eu.zonerama.com/MarieNezbedova/Album/12474446">
            Album Marie Nezbedové 2024
          </DropdownItem>
          <DropdownItem href="https://eu.zonerama.com/MarieNezbedova/Album/10384672">
            Album Marie Nezbedové 2023
          </DropdownItem>
          <DropdownItem href={site.gallery_url}>
            Starší alba na Rajče.net
          </DropdownItem>
        </ul>
      </li>

      <NavItem href={novinkyUrl}>Novinky</NavItem>

      <li
        className={cls("nav-item", "dropdown", {
          active: router.pathname.includes("/vysledky/"),
        })}
      >
        <Link
          href="#"
          className="nav-link dropdown-toggle"
          data-bs-toggle="dropdown"
          role="button"
          aria-haspopup="true"
          aria-expanded="false"
          id="vysledky-top-dropdown"
        >
          Výsledky <span className="caret"></span>
        </Link>
        <ul className="dropdown-menu" role="menu" aria-labelledby="vysledky-top-dropdown">
          {raceEventHeaders.map(({ id, year, viewType }) => (
            <DropdownItem href={getPath(year)} viewType={viewType} key={id}>
              {getTitle(year)}
            </DropdownItem>
          ))}
        </ul>
      </li>

      <li
        className={cls("nav-item", "dropdown", {
          active: [trasaZavoduUrl, kdoJsmeUrl, jakToCeleVznikloUrl].includes(router.pathname),
        })}
      >
        <Link
          href="#"
          className="nav-link dropdown-toggle"
          data-bs-toggle="dropdown"
          role="button"
          aria-haspopup="true"
          aria-expanded="false"
          id="navbarDropdownMenuLink"
        >
          O závodě <span className="caret"></span>
        </Link>
        <ul className="dropdown-menu" role="menu" aria-labelledby="navbarDropdownMenuLink">
          <DropdownItem href={trasaZavoduUrl}>Trasa závodu</DropdownItem>
          <DropdownItem href={kdoJsmeUrl}>Kdo jsme?</DropdownItem>
          <DropdownItem href={jakToCeleVznikloUrl}>Jak to celé vzniklo?</DropdownItem>
          {/*<DropdownItem href={zpracovaniOsonichUdajuUrl}>Zpracování osobních údajů</DropdownItem>*/}
          {/*<DropdownItem href={pravidlaZavoduUrl}>Pravidla závodu</DropdownItem>*/}
        </ul>
      </li>
    </ul>
  );
};

export default TopMenu;
