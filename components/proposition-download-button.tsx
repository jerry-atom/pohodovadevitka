import { FC } from "react";
import Link from "next/link";
import { fileUrl } from "../lib/fns";

type Props = {
  year: number;
};

const PropositionDownloadButton: FC<Props> = ({ year }) => (
  <Link
    href={fileUrl(`Propozice-${year}.pdf`)}
    className="btn btn-sm btn-secondary ms-2"
    rel="noopener"
    target="_blank"
    title="Stáhnout propozice jako PDF"
  >
    <i className="bi-download me-2" />
    Propozice
  </Link>
);

export default PropositionDownloadButton;
