import Head from "next/head";
import { FC } from "react";

type Props = {
  description: string;
  image?: string;
  title: string;
};

const TwitterCardSummary: FC<Props> = ({ description, image, title }) => (
  <Head>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@pohodovadevitka" />
    <meta name="twitter:creator" content="@pohodovadevitka" />
    <meta name="twitter:title" content={title} />
    <meta name="twitter:description" content={description} />
    {image && <meta name="twitter:image" content={image} />}
  </Head>
);

export default TwitterCardSummary;
