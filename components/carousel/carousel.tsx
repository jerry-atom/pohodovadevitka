import classNames from "classnames";
import Image from "next/image";
import { FC } from "react";

const pictures = [
  "https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_900,h_500/q_auto:eco/f_auto/ph9/logo",
  "https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_900,h_500/q_auto:eco/f_auto/ph9/phd-17",
  "https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_900,h_500/q_auto:eco/f_auto/ph9/phd-18",
  "https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_900,h_500/q_auto:eco/f_auto/ph9/phd-20",
  "https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_900,h_500/q_auto:eco/f_auto/ph9/phd-21",
  "https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_900,h_500/q_auto:eco/f_auto/ph9/phd-22",
];

const Carousel: FC = () => (
  <div id="carousel" className="carousel carousel-dark slide mb-3" data-bs-ride="carousel">
    <div className="carousel-inner">
      {pictures.map((picture, index) => (
        <div className={classNames(`carousel-item`, { active: index === 0 })} key={index}>
          <Image
            src={picture}
            alt="foto ze závodu"
            className="d-block mx-auto img-fluid rounded"
            height={500}
            width={900}
            priority={index === 0}
          />
        </div>
      ))}
    </div>
    <button
      className="carousel-control-prev"
      type="button"
      data-bs-target="#carousel"
      data-bs-slide="prev"
    >
      <span className="carousel-control-prev-icon" aria-hidden="true"></span>
      <span className="visually-hidden">Vlevo</span>
    </button>
    <button
      className="carousel-control-next"
      type="button"
      data-bs-target="#carousel"
      data-bs-slide="next"
    >
      <span className="carousel-control-next-icon" aria-hidden="true"></span>
      <span className="visually-hidden">Vpravo</span>
    </button>
  </div>
);

export default Carousel;
