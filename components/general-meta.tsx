import Head from "next/head";
import { FC } from "react";
import { site } from "../lib/constants";

type Props = {
  canonical?: string;
  description?: string;
  nextPath?: string;
  prevPath?: string;
};

const GeneralMeta: FC<Props> = (props) => (
  <Head>
    <meta name="description" content={props.description ?? site.description} />
    {props.canonical && <link rel="canonical" href={props.canonical} />}
    {props.prevPath && <link rel="prev" href={props.prevPath} />}
    {props.nextPath && <link rel="next" href={props.nextPath} />}
  </Head>
);

export default GeneralMeta;
