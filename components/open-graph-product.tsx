import Head from "next/head";
import { FC } from "react";

type Props = {
  description: string;
  image?: string;
  title: string;
  url: string;
};

const OpenGraphProduct: FC<Props> = ({ description, image, title, url }) => (
  <Head>
    <meta property="og:type" content="product" />
    <meta property="og:title" content={title} />
    <meta property="og:url" content={url} />
    {description && <meta property="og:description" content={description} />}
    {image && <meta property="og:image" content={image} />}
    <meta property="og:description" content={description} />
  </Head>
);

export default OpenGraphProduct;
