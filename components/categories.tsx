import { Category, Gender, Race } from "@prisma/client";
import { FC, Fragment } from "react";
import { groupById, uniq } from "../lib/fns";

type RaceRow = {
  categories: Record<Gender["id"], Category["id"] | null>;
  race?: { value: Race["id"]; rowSpan: number };
};

type CategoriesTable = {
  columns: Gender["id"][];
  rows: RaceRow[];
};

export function buildCategoriesTable(categories: Category[]): CategoriesTable {
  const genderIds = uniq(categories.map((category) => category.genderId));
  const raceIds = uniq(categories.map((category) => category.raceId));

  const raceRows: Record<Race["id"], Record<Gender["id"], Category["id"][]>> = raceIds.reduce(
    (a, raceId) => ({
      ...a,
      [raceId]: genderIds.reduce((b, genderId) => ({ ...b, [genderId]: [] }), {}),
    }),
    {},
  );

  for (const category of categories) {
    raceRows[category.raceId][category.genderId].push(category.id);
  }

  const rows: RaceRow[] = [];

  for (const [raceId, group] of Object.entries(raceRows)) {
    const rowSpan = Math.max(...Object.values(group).map((x) => x.length));

    for (let i = 0; i < rowSpan; i++) {
      const rowCategories = genderIds.reduce(
        (a, genderId) => ({ ...a, [genderId]: group[genderId][i] ?? null }),
        {},
      );

      if (i === 0) {
        rows.push({
          categories: rowCategories,
          race: { rowSpan, value: Number(raceId) },
        });
      } else {
        rows.push({ categories: rowCategories });
      }
    }
  }

  return {
    columns: genderIds,
    rows,
  };
}

const CategoryRow: FC<{ category: Category }> = ({ category }) => (
  <>
    <td>
      <span className="text-muted">{category.abbreviation}</span> {category.name}
    </td>
    <td>
      {category.yearFrom == null && category.yearTo == null ? "všechny" : null}
      {category.yearFrom == null && category.yearTo != null ? `${category.yearTo} a starší` : null}
      {category.yearFrom != null && category.yearTo == null
        ? `${category.yearFrom} a mladší`
        : null}
      {category.yearFrom != null && category.yearTo != null ? (
        <>
          {category.yearFrom} &ndash; {category.yearTo}
        </>
      ) : null}
    </td>
  </>
);

const EmptyCategoryRow: FC = () => (
  <>
    <td></td>
    <td></td>
  </>
);

type Props = {
  categories: Category[];
  genders: Gender[];
  races: Race[];
  title?: string;
};

const Categories: FC<Props> = ({ categories, genders, races, title }) => {
  const categoryById = groupById(categories);
  const genderById = groupById(genders);
  const raceById = groupById(races);
  const { columns, rows } = buildCategoriesTable(categories);

  return (
    <div>
      {title && <h2 className="my-3">{title}</h2>}
      <div className="table-responsive">
        <table className="table table-condensed table-bordered table-sm text-center align-middle">
          <thead>
            <tr>
              {columns.map((genderId) => (
                <th colSpan={2} key={genderId}>
                  {genderById[genderId].name}
                </th>
              ))}
              <th rowSpan={2}>ZÁVOD</th>
            </tr>
            <tr>
              {columns.map((genderId) => (
                <Fragment key={genderId}>
                  <th>Kategorie</th>
                  <th>Ročník narození</th>
                </Fragment>
              ))}
            </tr>
          </thead>
          <tbody>
            {rows.map((row, rowKey) => (
              <tr key={rowKey}>
                {columns
                  .map((genderId) => ({
                    categoryId: row.categories[genderId],
                    genderId,
                  }))
                  .map(({ categoryId, genderId }) => (
                    <Fragment key={genderId}>
                      {categoryId ? (
                        <CategoryRow category={categoryById[categoryId]} />
                      ) : (
                        <EmptyCategoryRow />
                      )}
                    </Fragment>
                  ))}
                {row.race && <td rowSpan={row.race.rowSpan}>{raceById[row.race.value].name}</td>}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Categories;
