import Head from "next/head";
import { FC } from "react";
import { site } from "../lib/constants";

type Props = { title: string };

const Title: FC<Props> = ({ title }) => (
  <Head>
    <title>{`${title} - ${site.title}`}</title>
  </Head>
);

export default Title;
