import { FC } from "react";
import Link from "next/link";
import { Post } from "../lib/posts";
import { pageUrl, convertDate } from "../lib/fns";

type Props = Post;

const PostCard: FC<Props> = ({ createdAt, title, path }) => (
  <div className="card h-100">
    <div className="card-header">
      <time dateTime={createdAt}>{convertDate(createdAt)}</time>
    </div>
    <div className="card-body">
      <p className="card-title">
        {title}
        <Link href={pageUrl(path)} className="ms-2">
          &hellip;přečíst
        </Link>
      </p>
    </div>
  </div>
);

export default PostCard;
