import { FC } from "react";
import Image from "next/image";
import Link from "next/link";
import TopMenu, { Props as TopMenuProps } from "../top-menu";

export type Props = TopMenuProps;

export const Header: FC<Props> = ({ raceEventHeaders }) => (
  <header className="d-print-none">
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-dark">
        <Link href="/" className="navbar-brand">
          <span className="d-block d-lg-none">
            <Image
              src="https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_34/q_auto:eco/f_webp/ph9/logo-top-dark-sm"
              alt="logo"
              title="PoHodová Devítka - běžecký závod Velké Bíteše"
              width={34}
              height={34}
            />
          </span>
          <span className="d-none d-lg-block">
            <Image
              src="https://res.cloudinary.com/dk1shqmyz/image/upload/c_scale,w_197/q_auto:eco/f_webp/ph9/logo-top-dark-lg"
              alt="logo"
              title="PoHodová Devítka - běžecký závod Velké Bíteše"
              width={197}
              height={45}
            />
          </span>
          <span className="visually-hidden">PoHodová Devítka - běžecký závod Velké Bíteše</span>
        </Link>

        <button
          type="button"
          className="navbar-toggler"
          data-bs-toggle="collapse"
          data-bs-target="#phd-navbar"
          aria-controls="phd-navbar"
          aria-expanded="false"
          aria-label="Přepnout navigaci"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse justify-content-end" id="phd-navbar">
          <TopMenu raceEventHeaders={raceEventHeaders} />
        </div>
      </nav>
    </div>
  </header>
);
