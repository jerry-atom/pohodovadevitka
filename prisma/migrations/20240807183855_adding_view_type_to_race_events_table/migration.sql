-- RedefineTables
PRAGMA defer_foreign_keys=ON;
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_RaceEvent" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "printProposition" BOOLEAN NOT NULL DEFAULT false,
    "printPropositionOriginal" BOOLEAN NOT NULL DEFAULT false,
    "year" INTEGER NOT NULL,
    "viewType" TEXT NOT NULL DEFAULT 'local'
);
INSERT INTO "new_RaceEvent" ("id", "printProposition", "printPropositionOriginal", "year") SELECT "id", "printProposition", "printPropositionOriginal", "year" FROM "RaceEvent";
DROP TABLE "RaceEvent";
ALTER TABLE "new_RaceEvent" RENAME TO "RaceEvent";
PRAGMA foreign_keys=ON;
PRAGMA defer_foreign_keys=OFF;
