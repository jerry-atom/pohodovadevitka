import { micromark } from "micromark";

export default function markdownToHtml(markdown: string): string {
  return micromark(markdown, "utf-8", { allowDangerousHtml: true });
}
