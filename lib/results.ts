import { PrismaClient } from "@prisma/client";
import { UnwrapArray } from "./fns";

const prisma = new PrismaClient();

export const getResultsByYear = async (year: number) =>
  prisma.result.findMany({
    orderBy: [
      {
        abs: "asc",
      },
      {
        cat: "asc",
      },
    ],
    where: {
      Category: {
        Race: {
          RaceEvent: {
            year,
          },
        },
      },
    },
    include: {
      user: {
        select: {
          name: true,
          year: true,
        },
      },
      Category: {
        select: {
          abbreviation: true,
        },
      },
    },
  });

export type Result = UnwrapArray<Awaited<ReturnType<typeof getResultsByYear>>>;
