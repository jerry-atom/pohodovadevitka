import { imageUrl, pageUrl } from "./fns";
import markdownToHtml from "./markdownToHtml";

export type Organizator = {
  id: string;
  name: string;
  description: string;
  picture: {
    original: string;
    scaled: { type: string; src: string }[];
  };
  gender: string;
  nationality: string;
  telephone?: string;
  email?: string;
  url: string;
};

const organizators: Organizator[] = [
  {
    id: "zdenda",
    name: "Zdeněk Kundera",
    description: `Běhá zhruba od roku 2007, od doby, kdy přestal hrát fotbal. Od roku 2011 člen [AC Moravské Slavie Brno](https://mslavia.cz/). Soupeři ho díky jeho výšce s oblibou nechávají tahat tempo, protože za ním je absolutní bezvětří. V současnosti koketuje s myšlenkou vyzkoušet duatlon, případně triatlon. Na plaveckou část triatlonu by ovšem potřeboval rukávky.

  **Další sporty**: cyklistika, běžky, tenis, badminton, fotbal
  **Oblíbení sportovci**: Ole Einar Bjoerndalen, Hicham ElGuerrouj, David Rudisha`,
    picture: {
      original: imageUrl("zdenda.jpg"),
      scaled: [
        { type: "image/jpg", src: imageUrl("zdenda-w200.jpg") },
        { type: "image/avif", src: imageUrl("zdenda-w200.webp") },
        { type: "image/webp", src: imageUrl("zdenda-w200.avif") },
      ],
    },
    gender: "male",
    nationality: "Czech Republic",
    url: pageUrl("/kdo-jsme/") + "#zdenda",
    telephone: "+420 777-612-344",
    email: "zkundera@seznam.cz",
  },
  {
    id: "martin",
    name: "Martin Macholán",
    description: `Běhu se začal věnovat cca před pěti lety a to na nátlak dlouholetého fotbalového parťáka a kamaráda &mdash; Zdeňka Kundery jednoho večera u oroseného pivečka v jedné nejmenované bítešské hospůdce. Jednoho dne se zkusil posadit na kolo a nespadl z něj. Potom si řekl, že by mohl zkusit přeplavat rybník a ku svému překvapení se neutopil. Na základě těchto úspěchů se začal věnovat triatlonu a nyní je členem triatlonového oddílu [Spartaku Třebíč](https://www.spartaktrebic.cz/), za který sbírá vavříny na poli cti a slávy.

  **Další sporty**: cyklistika, plavání, otužování, posilovna, fotbal
  **Oblíbení sportovci**: Emil Zátopek, Craig Alexander, Tony Martin, Michael Phelps`,
    picture: {
      original: imageUrl("martin.jpg"),
      scaled: [
        { type: "image/jpg", src: imageUrl("martin-w200.jpg") },
        { type: "image/avif", src: imageUrl("martin-w200.avif") },
        { type: "image/webp", src: imageUrl("martin-w200.webp") },
      ],
    },
    gender: "male",
    nationality: "Czech Republic",
    url: pageUrl("/kdo-jsme/") + "#martin",
  },
  {
    id: "ondra",
    name: "Ondřej Plechatý",
    description: `K běhání ho cca v roce 2009 přivedl velký kamarád, parťák, spoluorganizátor PoHodové devítky Zdeněk Kundera a probudil v něm doposud nepoznanou vášeň v tento nejpřirozenější sport. Kromě běhání se aktivně věnuje fotbalu, hraje za místní [bítešskou Benficu](http://fcvelkabites.com/tym/muzi-b/), zároveň je velkým fanouškem brněnské Komety. Kamarádi ho občas přirovnávají k naftovému motoru &mdash; pomalý rozjezd, drtivý finiš.

  **Další sporty**: fotbal, kolo, hokej, práce na baráčku
  **Oblíbení sportovci**: Emil Zátopek, Haile Gebrselassie, Paula Radcliffeová`,
    picture: {
      original: imageUrl("ondra.jpg"),
      scaled: [
        { type: "image/jpg", src: imageUrl("ondra-w200.jpg") },
        { type: "image/avif", src: imageUrl("ondra-w200.avif") },
        { type: "image/webp", src: imageUrl("ondra-w200.webp") },
      ],
    },
    gender: "male",
    nationality: "Czech Republic",
    url: pageUrl("/kdo-jsme/") + "#ondra",
    telephone: "+420 731-425-676",
  },
].map((x) => ({
  ...x,
  description: markdownToHtml(x.description),
}));

export const getOrganizators = async () => organizators;
