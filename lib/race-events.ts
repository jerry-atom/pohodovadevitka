import { Category, Gender, PrismaClient, Race, RaceEvent } from "@prisma/client";

const prisma = new PrismaClient();

export type RaceEventHeader = Pick<RaceEvent, "id" | "year" | "viewType">;

export const getRaceEventHeaders = async (): Promise<RaceEventHeader[]> =>
  prisma.raceEvent.findMany({
    select: {
      id: true,
      year: true,
      viewType: true,
    },
    orderBy: {
      year: "desc",
    },
  });

type ReducedSpecialCategory = {
  genderId: number;
  categoryName: string;
  userId: number;
  userName: string;
};

export type RaceEventData = {
  categories: Category[];
  genders: Gender[];
  races: Race[];
  raceEvent: RaceEvent;
  specialCategories: ReducedSpecialCategory[];
};

export async function getRaceEventData(year: number): Promise<RaceEventData> {
  const [raceEvent, categories, genders, races, specialCategories] = await Promise.all([
    prisma.raceEvent.findFirst({
      where: {
        year,
      },
    }),
    prisma.category.findMany({
      where: {
        Race: {
          RaceEvent: {
            year,
          },
        },
      },
    }),
    prisma.gender.findMany({
      where: {
        Category: {
          some: {
            Race: {
              RaceEvent: {
                year,
              },
            },
          },
        },
      },
    }),
    prisma.race.findMany({
      where: {
        RaceEvent: {
          year,
        },
      },
    }),
    prisma.specialCategory.findMany({
      where: {
        raceEvent: {
          year,
        },
      },
      select: {
        genderId: true,
        name: true,
        user: {
          select: {
            id: true,
            name: true,
          },
        },
      },
      orderBy: [
        {
          genderId: "asc",
        },
        {
          position: "asc",
        },
      ],
    }),
  ]);

  if (raceEvent == null) {
    throw new Error("RaceEvent not found");
  }

  return {
    categories,
    genders,
    races,
    raceEvent,
    specialCategories: specialCategories.map((x) => ({
      categoryName: x.name,
      genderId: x.genderId,
      userId: x.user.id,
      userName: x.user.name,
    })),
  };
}
