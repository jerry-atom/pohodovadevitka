import { parseISO, format } from "date-fns";
import { cs } from "date-fns/locale";

import { ORIGIN } from "./constants";

type Primitives = string | number | symbol | boolean;

export function uniq<T extends Primitives>(xs: T[]): T[] {
  return Array.from(new Set(xs).values());
}

export function pageUrl(path: string = "/"): string {
  const url = new URL(ORIGIN);
  if (path.length === 0) {
    url.pathname = "";
  } else if (path.endsWith("/")) {
    url.pathname = path;
  } else {
    url.pathname = path + "/";
  }
  return url.toString();
}

export function fileUrl(fileName: string): string {
  const url = new URL(ORIGIN);
  url.pathname = `/files/${fileName}`;
  return url.toString();
}

export function imageUrl(fileName: string): string {
  const url = new URL(ORIGIN);
  url.pathname = `/images/${fileName}`;
  return url.toString();
}

export function groupById<K extends number, T extends { id: K }>(xs: T[]): Record<K, T> {
  return xs.reduce((a, x) => ({ ...a, [x.id]: x }), {} as Record<K, T>);
}

export type UnwrapArray<T> = T extends (infer U)[] ? U : never;

export function convertDate(isoDate: string): string {
  const dateObject = parseISO(isoDate);
  return format(dateObject, "d.M.y", { locale: cs });
}

export function convertDateTime(isoDate: string): string {
  const dateObject = parseISO(isoDate);
  return format(dateObject, "d.M.y H:mm", { locale: cs });
}
