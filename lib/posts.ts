import { PrismaClient } from "@prisma/client";
import markdownToHtml from "./markdownToHtml";

const prisma = new PrismaClient();

export type Post = {
  content: string;
  createdAt: string;
  description: string;
  nextPath?: string;
  path: string;
  prevPath?: string;
  published: boolean;
  slug: string;
  title: string;
};

const processPost = (x: Post): Post => ({
  ...x,
  content: markdownToHtml(x.content),
});

const processPostList = (xs: Post[]): Post[] => xs.map(processPost);

async function getPosts(): Promise<Post[]> {
  const dbPosts = await prisma.post.findMany({
    where: {
      published: true,
    },
    orderBy: {
      createdAt: "desc",
    },
  });

  return dbPosts.map((x, i, a) => {
    const y: Post = {
      content: x.content,
      createdAt: x.createdAt.toISOString(),
      description: x.description,
      path: x.path,
      published: x.published,
      slug: x.slug,
      title: x.title,
    };
    if (i > 0) {
      y.nextPath = a[i - 1].path;
    }
    if (i < a.length - 1) {
      y.prevPath = a[i + 1].path;
    }
    return y;
  });
}

export async function getAllPosts(): Promise<Post[]> {
  return processPostList(await getPosts());
}

export async function getLastPosts(count: number): Promise<Post[]> {
  return processPostList((await getPosts()).slice(0, count));
}

export async function getPostByPath(path: string): Promise<Post> {
  const post = (await getPosts()).find((x) => x.path === path);
  if (!post) {
    throw Error(`Post ${path} not found`);
  }
  return processPost(post);
}
