/**
 * There is no trailing slash in ORIGIN!
 */
export const ORIGIN = process.env.SITE_URL ?? "https://www.pohodovadevitka.cz";

export const site = {
  title: "Pohodová devítka",
  email: "bitesskybeh@seznam.cz",
  author_email: "jaroslav.novotny.84@gmail.com",
  facebook_url: "https://www.facebook.com/pohodovadevitka",
  twitter_url: "https://twitter.com/pohodovadevitka",
  twitter_user: "@pohodovadevitka",
  gallery_url: "https://pohodovadevitka.rajce.idnes.cz",
  description:
    "Pohodová Devítka je běžecký závod ve Velké Bíteši, který se pořádá v září, vždy po zdejších hodech. Hlavní trasa měří 9km, děti mohou běžet kratší trasy.",
};
