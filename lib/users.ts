import { PrismaClient, Result, User, SpecialCategory } from "@prisma/client";

const prisma = new PrismaClient();

export type UserProfileHeader = Pick<User, "id" | "name">;

export type UserProfile = UserProfileHeader & {
  results: (Result & {
    categoryName: string;
    genderName: string;
    raceName: string;
    raceEventYear: number;
    specialCategories: SpecialCategory[];
  })[];
};

export const getUserProfiles = async () =>
  prisma.user.findMany({
    select: {
      id: true,
      name: true,
    },
  });

export const getUserProfile = async (id: number): Promise<UserProfile> =>
  prisma.user
    .findUnique({
      where: {
        id,
      },
      include: {
        Result: {
          orderBy: {
            Category: {
              Race: {
                RaceEvent: {
                  year: "desc",
                },
              },
            },
          },
          include: {
            Category: {
              select: {
                name: true,
                gender: {
                  select: {
                    name: true,
                  },
                },
                Race: {
                  select: {
                    name: true,
                    RaceEvent: {
                      select: {
                        year: true,
                        SpecialCategory: {
                          where: {
                            user: {
                              id,
                            },
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    })
    .then((x) => ({
      id: x!.id,
      name: x!.name,
      results: x!.Result.map((result) => ({
        ...result,
        categoryName: result.Category.name,
        genderName: result.Category.gender.name,
        raceName: result.Category.Race!.name,
        raceEventYear: result.Category.Race!.RaceEvent!.year,
        specialCategories: result.Category.Race?.RaceEvent?.SpecialCategory ?? [],
      })),
    }));
