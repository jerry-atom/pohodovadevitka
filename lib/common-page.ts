import { GetStaticProps } from "next";
import { FC } from "react";

import { getRaceEventHeaders } from "./race-events";
import { Props as LayoutProps } from "../components/layout";

export type CommonPageProps<T = {}> = T & LayoutProps;
export type CommonPage<T = {}> = FC<CommonPageProps<T>>;

export const getCommonPageStaticProps: GetStaticProps<CommonPageProps> = () =>
  getRaceEventHeaders().then((raceEventHeaders) => ({
    props: { raceEventHeaders },
  }));
